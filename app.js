require("dotenv").config();
const bodyParser = require("body-parser");
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const session = require("express-session");
var logger = require('morgan');
// const database = require('./database/server');
const cors = require('cors'); // addition we make
const fileUpload = require('express-fileupload'); //addition we make
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const uuidv4 = require('uuid').v4;
const functions = require('firebase-functions')
const admin = require('firebase-admin');
const e = require("express");

// admin.initializeApp(functions.config().firebase)

const serviceAccount = require('./serviceAccountKey.json');
//initialize admin SDK using serciceAcountKey
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount)
});
const db = admin.firestore();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(session({
  secret: uuidv4(),
  resave: false,
  saveUninitialized: true,
}))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);

// Use JSON parser for all non-webhook routes
app.use((req, res, next) => {
  if (req.originalUrl === "/webhook") {
    next();
  } else {
    bodyParser.json()(req, res, next);
  }
});

//Using CORS and File Upload modules here
app.use(cors());
app.use(fileUpload());

app.get("/get-oauth-link", async (req, res) => {
  const state = uuidv4();
  req.session.state = state
  const args = new URLSearchParams({
    state,
    client_id: process.env.STRIPE_CLIENT_ID,
    scope: "read_write",
    response_type: "code",
  })
  const url = `https://connect.stripe.com/oauth/authorize?${args.toString()}`;
  return res.send({ url });
});

app.get("/authorize-oauth", async (req, res) => {
  const { code, state } = req.query;
authorize
  // Assert the state matches the state you provided in the OAuth link (optional).
  if (req.session.state !== state) {
    return res.status(403).json({ error: 'Incorrect state parameter: ' + state });
  }

  // Send the authorization code to Stripe's API.
  stripe.oauth.token({
    grant_type: 'authorization_code',
    code
  }).then(
    (response) => {
      let connectedAccountId = response.stripe_user_id;
      saveAccountId(connectedAccountId);
      // Render some HTML or redirect to a different page.
      return res.redirect(301, 'http://localhost:3000') 
    },
    (err) => {
      if (err.type === 'StripeInvalidGrantError') {
        return res.status(400).json({ error: 'Invalid authorization code: ' + code });
      } else {
        return res.status(500).json({ error: 'An unknown error occurred.' });
      }
    }
  );
});

const saveAccountId = (id) => {
  // Save the connected account ID from the response to your database.
  console.log('Connected account ID: ' + id);
}


//Deauthorize
app.get("/get-oauth-deauthorize", async(req, res) => {

  const url1 = `https://connect.stripe.com/oauth/deauthorize`;
  res.send({ url1 });
  stripe.oauth.deauthorize({
    grant_type: 'authorization_code',
    client_id: process.env.STRIPE_CLIENT_ID,
    connect_id: stripe_user_id,
    code
  }).then(
    (response) => {
      let disconnected_account_id = response.stripe_user_id;
      unsaveAccountId(disconnected_account_id);
      // Render some HTML or redirect to a different page.
      return res.redirect(301, 'http://localhost:3000') 
    },
    (err) => {
      if (err.type === 'StripeInvalidGrantError') {
        return res.status(400).json({ error: 'Invalid authorization code: ' + code });
      } else {
        return res.status(500).json({ error: 'An unknown error occurred.' });
      }
    }
  );
});

const unsaveAccountId = (id) => {
  // Save the connected account ID from the response to your database.
  console.log('Disconnected account ID: ' + id);
}

app.post('/payment', function (req, res) {
  console.log("posted");
  var token = req.body.stripeToken;
  var email = req.body.email;
  var chargeAmount = req.body.chargeAmount;
  var charge = stripe.charges.create({
    amount: chargeAmount,
    email: email,
    currency: "gbp",
    source: token,
  },{
    stripeAccount: process.env.STRIPE_CONNECTED_ID,
    //stripeAccount: this.stripe_user_id,
  },
  function(err, charge){
    if(err & err.type === "StripeCardError"){
        console.log("Your card was decliend");
    }
  }).then(function getData(){
      return new Promise(function(resolve, reject) {
      resolve({
        "amount": chargeAmount,
        "email": email,
        "Description": "Hyper Car",
        "currency": currency,
        "source": token,
      });
    }).then(result =>{
      console.log(result);
      const obj = result;
      const Data = {
        amount: obj.amount,
        Description: obj.Description,
        currency: obj.currency,
        source: obj.source
      };
      return  db.collection('sampleData').doc('payment details')
      .set(Data).then(() => 
      console.log('new Dialogue written to database'));
    });
  })
  return res.redirect('/');


});































/*
placeOrderButtonPressed = () =>{
  var token = req.body.stripeToken;
  var chargeAmount = req.body.chargeAmount;
  var idempotency_key = uuid();
  //const {email} = req.body;
  const customer = stripe.customer.create({
    email: token.email,
    source: token.id
  });

  var charge = stripe.charges.create({
    amount: chargeAmount,
    currency: "gdp",
    customer: customer.id,
    receipt_email: token.email,
    description: 'Buy our car',

    source: token,
  },
  { 
    stripeAccount: process.env.STRIPE_CONNECTED_ID, //Using Direct payment flow for standerd account
  },
    //this.saveDataToFireStore(stripeObject),
  function(err, charge){
    if(err & err.type === "StripeCardError"){
      console.log("Your card was decliend");
    }
  });
  console.log("Your payment was successful")
}

//Accept Payment
app.post('/payment', function(req, res){
  placeOrderButtonPressed().then(result => {
    console.log(result);
    const obj = result;
    const charge = {
      amount: obj.amount,
      Description: obj.Description,
      currency: obj.currency,
      source: obj.source
    };
    return debug.collection('sampleData').doc('payment details')
    .set(charge).then(() => 
    console.log('new database is been written to database'));
  }); 
  
  /*
  var token = req.body.stripeToken;
  var chargeAmount = req.body.chargeAmount;
  
  var charge = stripe.charges.create({
    amount: chargeAmount,
    currency: "gdp",
    source: token,
  },
  { 
    stripeAccount: process.env.STRIPE_CONNECTED_ID, //Using Direct payment flow for standerd account
  },
    //this.saveDataToFireStore(stripeObject),
  function(err, charge){
    if(err & err.type === "StripeCardError"){
      console.log("Your card was decliend");
    }
  });
  //res.redirect('http://localhost:3000/payment');
  */
//});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;